<?php
namespace Tests\Feature;

use Tests\TestCase;

/**
 * Class UpgradeCommandTest.
 *
 * @author Johan Alvarez <llstarscreamll@hotmail.com>
 */
class UpgradeCommandTest extends TestCase
{
    /**
     * @var string
     */
    private $outputPath = 'storage/app/upgraded_app_test/';

    /**
     * @var string
     */
    private $inputPath = 'tests/_data/example_app/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->deleteTestDir();

        // change target app folder to the example one
        config(['upgrader.input_path' => $this->inputPath]);
        config(['upgrader.output_path' => $this->outputPath]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        // $this->deleteTestDir();
    }

    private function deleteTestDir()
    {
        $outputDir = base_path($this->outputPath);

        if (file_exists($outputDir)) {
            exec("rm -rf $outputDir");
        }
    }

    /**
     * Prueba actualizar los modelos, deben ser movidos a la carpeta Models y
     * los directorios hijos deben tener en su nombre la primera letra en
     * mayúscula y todos los namespaces deben estar corregidos.
     *
     * @test
     * @return void
     */
    public function testToUpgradeModels()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading models: ✔')
             ->assertExitCode(0);

        // los modelos deben estar correctamente ubicados
        $modelsPath = $this->outputPath.'app/Models';
        $this->assertTrue(file_exists(base_path($modelsPath.'/User.php')), 'User model exists');
        $this->assertTrue(file_exists(base_path($modelsPath.'/Orders/Order.php')), 'Order model exists');

        // los namespaces de los modelos a los que se refiera en las relaciones
        // deben estar actualizados acorde a la nueva estructura y deben tener
        // el prefijo App\Models\
        $userModelContents          = file_get_contents($this->outputPath.'app/Models/User.php');
        $orderModelContents         = file_get_contents($this->outputPath.'app/Models/Orders/Order.php');
        $userCreditModelContents         = file_get_contents($this->outputPath.'app/Models/UserCredit.php');
        $authContractsUseStatements = [
            'use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;',
            'use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;',
            'use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;',
        ];
        $authContracts = 'implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract';
        $traits        = implode(', ', [
            '\Illuminate\Auth\Authenticatable',
            '\Illuminate\Auth\Passwords\CanResetPassword',
            '\Illuminate\Foundation\Auth\Access\Authorizable',
            '\Illuminate\Notifications\Notifiable',
        ]);
        $authTraits = "use \App\Models\Users\UserAttributes, $traits;";

        foreach ($authContractsUseStatements as $useStatement) {
            $this->assertTrue(str_contains($userModelContents, $useStatement), 'auth contracts use fixed');
        }

        $this->assertTrue(str_contains($userModelContents, $authContracts), 'authenticatable contracts fixed');
        $this->assertTrue(str_contains($userModelContents, $authTraits), 'authenticatable traits fixed');
        $this->assertTrue(str_contains($userModelContents, 'catch (\Exception $e)'), 'catch Exception namespace fixed');
        $this->assertTrue(str_contains($userModelContents, 'hasMany(orders\Order::class'), 'string namespaces on hasMany fixed');
        $this->assertTrue(str_contains($userModelContents, 'hasMany(UserAddress::class'), '::class namespaces on hasMany fixed');
        $this->assertTrue(str_contains($userCreditModelContents, 'hasMany(User::class'), '\Root::class namespaces on hasMany fixed');
        $this->assertTrue(str_contains($userModelContents, 'belongsTo(User::class'), 'string namespaces on belongsTo fixed');
        $this->assertTrue(str_contains($userModelContents, 'belongsTo(Role::class'), 'namespaces on belongsTo fixed');
        $this->assertTrue(str_contains($userModelContents, 'hasOne(TicketCall::class'), 'namespaces on belongsTo hasOne');
        $this->assertTrue(str_contains($userModelContents, 'hasOne(Ticket::class'), 'namespaces on belongsTo hasOne');
        $this->assertTrue(str_contains($userModelContents, 'new \App\Models\Orders\Order()'), 'namespaces on new instances fixed');
        $this->assertTrue(str_contains($userModelContents, ' \App\Models\UserCredit::'), 'namespaces on static calls fixed');
        $this->assertTrue(str_contains($userModelContents, 'namespace App\Models;'), 'root files namespaces fixed');
        $this->assertTrue(str_contains($orderModelContents, 'namespace App\Models\Orders;'), 'nested paths files namespaces fixed');
        $this->assertTrue(str_contains($userModelContents, "->pluck('warehouse','id');"), 'lists() changed to pluck()');
        $this->assertTrue(str_contains($userModelContents, "newPivot(Model \$parent, array \$attributes, \$table, \$exists, \$using = null)"), 'newPivot method extension arguments fixed');
    }

    /**
     * Prueba actualizar los controladores.
     *
     * @test
     * @return void
     */
    public function testToUpgradeControllers()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading controllers: ✔')
             ->assertExitCode(0);

        // los controladores deben estar correctamente ubicados
        $controllersPath = $this->outputPath.'app/Http/Controllers';

        $this->assertTrue(file_exists(base_path($controllersPath.'/UsersController.php')), 'Users controller exists');

        $userControllerContents = file_get_contents(base_path($controllersPath.'/UsersController.php'));
        $apiControllerContents  = file_get_contents(base_path($controllersPath.'/Api/ApiController.php'));
        $otherControllerContents  = file_get_contents(base_path($controllersPath.'/Api/Other/AnotherController.php'));

        $this->assertTrue(str_contains($userControllerContents, '$this->middleware(function ($incomingRequest, $next) {'), 'beforeFilter changed to middleware with arguments');
        $this->assertTrue(str_contains($userControllerContents, "return \$next(\$incomingRequest);\n},"), 'before filter return fixed');
        $this->assertTrue(str_contains($apiControllerContents, "\$incomingRequestResponse = \$next(\$incomingRequest);"), 'after filter request handled fixed');
        $this->assertTrue(str_contains($apiControllerContents, "return \$incomingRequestResponse;"), 'after filter return fixed');
        $this->assertTrue(str_contains($userControllerContents, 'namespace App\Http\Controllers;'), 'root files namespaces fixed');
        $this->assertTrue(str_contains($apiControllerContents, 'new \App\Models\User('), 'new model instances fixed');
        $this->assertTrue(str_contains($apiControllerContents, '\App\Models\User::'), 'static model calls fixed');
        $this->assertTrue(str_contains($apiControllerContents, 'namespace App\Http\Controllers\Api;'), 'nested files namespace fixed');
        $this->assertTrue(str_contains($apiControllerContents, 'env('), 'getenv() to env() changed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Request::get(\''), 'Input::get(...) fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Request::all()'), 'Input::get() fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Request::filled('), 'Input::has() fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Request::merge('), 'Input::merge() fixed');
        $this->assertTrue(str_contains($userControllerContents, 'class UsersController extends \App\Http\Controllers\BaseController'), 'root controller extends fixed');
        $this->assertTrue(str_contains($apiControllerContents, 'class ApiController extends \App\Http\Controllers\BaseController'), 'nested controller extends fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\URL::'), 'URL facade namespace fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Request::is('), 'Request facade namespace fixed');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Session::put(\'error\', null);'), 'Session::set changed to Session::put');
        $this->assertTrue(str_contains($otherControllerContents, 'use App\Http\Controllers\Api\ApiController;'), 'namespaces on top use statements fixed');

        // casos específico para Merqueo
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Auth::guard(\'admin\')->user('), 'AuthBackend facade changed to Auth::guard("admin")');
        $this->assertTrue(str_contains($userControllerContents, '\Illuminate\Support\Facades\Auth::guard(\'admin\')->check('), 'BackendAuth facade changed to Auth::guard("admin")');
        $this->assertTrue(str_contains($userControllerContents, "preg_replace(['/admin\\\\\\/', '/^\\\\\\/'], '', \$request['controller']);"), 'regex updated');
        $this->assertTrue(str_contains($userControllerContents, "\$receptions->render()->toHtml();"), 'paginator render calls on classes fixed');
    }

    /**
     * Prueba actualizar las excepciones.
     *
     * @test
     * @return void
     */
    public function testToUpgradeExceptions()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading exceptions: ✔')
             ->assertExitCode(0);

        // las excepciones deben estar correctamente ubicados
        $controllersPath = $this->outputPath.'app/Exceptions';

        $this->assertTrue(file_exists(base_path($controllersPath.'/CouponException.php')), 'CouponException exists');

        $exception = file_get_contents(base_path($controllersPath.'/CouponException.php'));

        $this->assertTrue(str_contains($exception, 'extends \Exception'), 'Exception extend namespace fixed');
        $this->assertTrue(str_contains($exception, 'namespace App\Exceptions;'), 'root files namespaces fixed');
    }

    /**
     * Prueba actualizar archivos dentro de carpeta clases.
     *
     * @test
     * @return void
     */
    public function testToUpgradeClasses()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading classes: ✔')
             ->assertExitCode(0);

        // las excepciones deben estar correctamente ubicados
        $classesPath = $this->outputPath.'app/Classes';

        $this->assertTrue(file_exists(base_path($classesPath.'/Volumetry.php')), 'Volumetry exists');

        $class = file_get_contents(base_path($classesPath.'/Volumetry.php'));

        $this->assertTrue(str_contains($class, 'catch (\Exception $e)'), 'Exception namespace on catch fixed');
        $this->assertTrue(str_contains($class, 'namespace App\Classes;'), 'root files namespaces fixed');
        $this->assertTrue(str_contains($class, ' \App\Models\User::'), 'models static calls fixed');
        $this->assertTrue(str_contains($class, ' \Illuminate\Support\Facades\DB::'), 'DB static calls fixed');
    }

    /**
     * Prueba actualizar comandos.
     *
     * @test
     * @return void
     */
    public function testToUpgradeCommands()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading commands: ✔')
             ->assertExitCode(0);

        // los comandos deben estar correctamente ubicados
        $commandsPath = $this->outputPath.'app/Console/Commands';

        $this->assertTrue(file_exists(base_path($commandsPath.'/CouponsGenerator.php')), 'CouponsGenerator exists');

        $exception = file_get_contents(base_path($commandsPath.'/CouponsGenerator.php'));

        $this->assertTrue(str_contains($exception, 'public function handle('), 'fire method changed to handle');
        $this->assertTrue(str_contains($exception, 'namespace App\Console\Commands;'), 'command namespace fixed');
        $this->assertTrue(str_contains($exception, '\App\Models\User::'), 'models static calls fixed');
        $this->assertTrue(str_contains($exception, 'new \App\Models\Orders\Order;'), 'new models instances fixed');
    }

    /**
     * Prueba actualizar eventos.
     *
     * @test
     * @return void
     */
    public function testToUpgradeEvents()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading events: ✔')
             ->assertExitCode(0);

        // los comandos deben estar correctamente ubicados
        $commandsPath = $this->outputPath.'app/Events';

        $this->assertTrue(file_exists(base_path($commandsPath.'/InvestigationStockEventHandler.php')), 'InvestigationStockEventHandler exists');

        $exception = file_get_contents(base_path($commandsPath.'/InvestigationStockEventHandler.php'));

        $this->assertTrue(str_contains($exception, 'namespace App\Events;'), 'event namespace fixed');
        $this->assertTrue(str_contains($exception, ' \App\Models\User $'), 'models type hint fixed');
        $this->assertTrue(str_contains($exception, ' \App\Models\UserCredit $'), 'models type hint fixed');
        $this->assertTrue(str_contains($exception, ' \App\Models\Orders\Order $'), 'models type hint fixed');
        $this->assertTrue(str_contains($exception, 'new \App\Models\Orders\Order;'), 'new models instances fixed');
        $this->assertTrue(str_contains($exception, 'App\Events\InvestigationStockEventHandler@onUpdatePickingQuantity'), 'namespaces on listen methods fixed');
        $this->assertTrue(str_contains($exception, 'App\Events\InvestigationStockEventHandler@onUpdateStorageQuantity'), 'namespaces on listen methods fixed');
    }

    /**
     * Prueba actualizar eventos.
     *
     * @test
     * @return void
     */
    public function testToUpgradeObservers()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading observers: ✔')
             ->assertExitCode(0);

        // los comandos deben estar correctamente ubicados
        $commandsPath = $this->outputPath.'app/Observers';

        $this->assertTrue(file_exists(base_path($commandsPath.'/StoreProductGroupObserver.php')), 'StoreProductGroupObserver exists');

        $observer = file_get_contents(base_path($commandsPath.'/StoreProductGroupObserver.php'));

        $this->assertTrue(str_contains($observer, 'namespace App\Observers;'), 'observer namespace fixed');
        $this->assertTrue(str_contains($observer, '(\App\Models\User $'), 'models type hint fixed');
    }

    /**
     * Prueba actualizar vistas.
     *
     * @test
     * @return void
     */
    public function testToUpgradeViews()
    {
        $this->artisan('upgrade')
             ->expectsOutput('Upgrading views: ✔')
             ->assertExitCode(0);

        // los comandos deben estar correctamente ubicados
        $viewsPath = $this->outputPath.'resources/views';

        $this->assertTrue(file_exists(base_path($viewsPath.'/welcome.blade.php')), 'welcome view exists');

        $view        = file_get_contents(base_path($viewsPath.'/welcome.blade.php'));
        $actionFixed = "action('\App\Http\Controllers\Api\ApiController@assign_services_ajax'";

        $this->assertTrue(str_contains($view, '{{ foo(\App\Http\Controllers\Api\Other\AnotherController::FOO) }}'), 'static calls fixed on views');
        $this->assertTrue(str_contains($view, '\'form-control\']) }}'), 'last boolean arg to Form::select fixed');
        $this->assertTrue(str_contains($view, '@csrf'), 'csrf token on post forms fixed');
        $this->assertTrue(str_contains($view, $actionFixed), 'namespaces on action calls fixed');
        $this->assertTrue(str_contains($view, "{{ \$foo }}"), '{{ $foo; }} fixed');
        $this->assertTrue(str_contains($view, "{{\$bar}}"), '{{$bar;}} fixed');
        $this->assertTrue(str_contains($view, "@if((is_array(\$stores) || \$stores instanceof Countable) && count(\$stores))"), 'count on no countable fixed');
        $this->assertTrue(str_contains($view, "{{ \$users->render() }}"), '$paginator->links() changed to $paginator->render()');
        $this->assertTrue(str_contains($view, '<meta name="csrf-token" content="{{ csrf_token() }}">'), 'csrf token header added');
        $this->assertTrue(str_contains($view, '{!! $cities->toJson() !!}'), 'json scaped');
        $this->assertTrue(str_contains($view, '{{ is_array($users) || $users instanceof Countable ? count($users) : 0 }}'), 'printed counts that are not countable fixed');
    }
}
