<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CouponsGenerator extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:generate-coupons';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generador de cupones.';


    /**
     * Errores del compando
     *
     * @var array
     */
    protected $errors = [];

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $main_string = $this->argument('main_string');
        $coup_cant = $this->argument('coup_cant');
        $digit_length = $this->argument('digit_length');
        $digit_type = $this->argument('digit_type');
        $campaign_validation = $this->argument('campaign_validation');
        $retry = 0;
        for ($i = 0; $i < $coup_cant; $i++) {
            $coupon_obj = User::where('code', $code)->first();

            if (empty($coupon_obj)) {
                $coupon_obj = new \orders\Order;
                $coupon_obj->type = 'Credit Amount';
                $coupon_obj->code = $code;
                $coupon_obj->save();
            } elseif ($retry != 3) {
                $i--;
                $retry++;
            }
        }

        $this->info("Se han creado: $i cupones y se han hecho $retry reintentos.");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
	    return [
            ['coup_cant', InputArgument::REQUIRED, 'Cantidad de cupones'],
            ['digit_type', InputArgument::REQUIRED, 'Tipo de cupon, número con cadena'],
            ['digit_length', InputArgument::REQUIRED, 'Largo de la cadena o número'],
            ['campaign_validation', InputArgument::REQUIRED, 'Nombre de la campaña'],
            ['main_string', InputArgument::OPTIONAL, 'Prefijo del cupón'],
        ];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
