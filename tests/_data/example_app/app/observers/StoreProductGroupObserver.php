<?php

/**
 * Class StoreProductGroupObserver
 */
class StoreProductGroupObserver
{
    /**
     * @param User $store_product_group
     * @throws Exception
     */
    public function created(User $store_product_group)
    {
        $this->updateParent($store_product_group);
    }

    /**
     * @param User $store_product_group
     * @throws Exception
     */
    public function deleted(User $store_product_group)
    {
        $this->updateParent($store_product_group);
    }

    /**
     * @param User $store_product_group
     * @throws Exception
     */
    private function updateParent(User $store_product_group)
    {
        $store_product = $store_product_group->storeProduct;
        if ($store_product) {
            $store_product->calculatePriceOnGroup();
            $store_product->save();
        }
    }
}
