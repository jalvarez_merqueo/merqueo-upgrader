<?php
/**
* Clase para manejar eventos de recibos de bodega
*/
class InvestigationStockEventHandler
{

    public function onUpdatePickingQuantity(
        User $inventoryCountingPositionDetail,
        UserCredit $storeProductWarehouse,
        \orders\Order $inventoryCountingPosition
    ) {
        $difference = $inventoryCountingPositionDetail->picking_counted - $storeProductWarehouse->picking_stock;

        $investigationStock = new \orders\Order;
        $investigationStock->inventory_counting_position_id = $inventoryCountingPosition->id;
        $investigationStock->store_product_id = $storeProductWarehouse->store_product_id;
        $investigationStock->warehouse_id = $inventoryCountingPosition->warehouse_id;
        $investigationStock->storage = $inventoryCountingPosition->action;
        $investigationStock->position = $inventoryCountingPositionDetail->position;
        $investigationStock->height_position = $inventoryCountingPositionDetail->position_height;
        $investigationStock->quantity = $difference;
        $investigationStock->save();
    }

    public function onUpdateStorageQuantity(
        User $inventoryCountingPositionDetail,
        UserCredit $warehouseStorage,
        \orders\Order $inventoryCountingPosition
    ) {
        $difference = $inventoryCountingPositionDetail->quantity_counted - $warehouseStorage->quantity;

        $investigationStock = new UserCredit;
        $investigationStock->inventory_counting_position_id = $inventoryCountingPosition->id;
        $investigationStock->store_product_id = $warehouseStorage->store_product_id;
        $investigationStock->warehouse_id = $inventoryCountingPosition->warehouse_id;
        $investigationStock->storage = $inventoryCountingPosition->action;
        $investigationStock->position = $inventoryCountingPositionDetail->position;
        $investigationStock->height_position = $inventoryCountingPositionDetail->position_height;
        $investigationStock->quantity = $difference;
        $investigationStock->save();
    }

    /**
     * Suscribe los eventos.
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'investigationStock.onUpdatePickingQuantity',
            'InvestigationStockEventHandler@onUpdatePickingQuantity'
        );
        $events->listen('investigationStock.onUpdateStorageQuantity','InvestigationStockEventHandler@onUpdateStorageQuantity');
    }
}