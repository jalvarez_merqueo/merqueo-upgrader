<?php

namespace api;

class ApiController extends \BaseController
{
    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->afterFilter(function() {
            // after filter start
            Request::is('api/transporter/1.0/*');
            // after filter end
        }, array('except' => array('init')));
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $newUser = new User(['name' => 'John']);
        $data[] = User::all();
        $foo = getenv('SOME_VAR');

        return view('some.view', $data);
    }
}
