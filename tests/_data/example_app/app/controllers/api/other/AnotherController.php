<?php

namespace api\other;

use api\ApiController;

class AnotherController extends ApiController
{
    /**
     * @return mixed
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $newUser = new User(['name' => 'John']);
        $data[] = User::all();
        $foo = getenv('SOME_VAR');

        return view('some.view', $data);
    }
}
