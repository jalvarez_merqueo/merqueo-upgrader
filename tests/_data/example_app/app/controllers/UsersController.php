<?php

class UsersController extends BaseController
{
    /**
     * @return mixed
     */
    public function __construct()
    {
        parent::__construct();

        $this->beforeFilter(
            function()
        {
            Request::is('api/transporter/1.0/*');
            Input::merge(['app_version' => $this->app_version]);
            Input::has('user_id');
            Input::get('user_id');
            Input::get();
            Request::segment(3) === 'user';
            Route::current()->parameter('id');
            Config::get('app.api.public_key');
            Request::getPathInfo();
            Event::subscribe(new OrderEventHandler());
            URL::current();
            AuthBackend::user();
            BackendAuth::check();
            Session::set('error', null);
            str_replace('admin\\', '', $request['controller']);
            $receptions->links()->render();

            if (!Auth::guard('admin')->check() && !Request::filled('is_webservice')){
                return Redirect::route('admin.login');
            } else {
                $user = Auth::guard('admin')->user();

                $admin_url = env('URL_ADMIN') ?: 'admin';
                if (empty($user->status) && Request::path() !== "$admin_url/logout") {
                    return Redirect::action('admin.logout');
                }

                if ($user->update_menu) {
                    $this->menu_tree($user);
                    $user->update_menu = FALSE;
                    $user->save();
                }

                $request = get_current_request();
                $request['controller'] = preg_replace(['/admin\\\/', '/^\\\/'], '', $request['controller']);
                $admin_permissions = json_decode(Session::get('admin_permissions'), true);
                //debug($admin_permissions);

                $found_index = strpos($request['controller'], 'report\\');
                if (!Request::filled('is_webservice'))
                {
                    //validar acceso - controlador
                    if ($found_index === false && $found_index !== 0) {
                        if (!isset($admin_permissions[$request['controller']]))
                            die('Acceso denegado');

                        //validar acceso - action
                        if (is_array($admin_permissions[$request['controller']]['actions'])){
                            if (!in_array($request['action'], $admin_permissions[$request['controller']]['actions']))
                                die('Acceso denegado');
                        }else die('Acceso denegado');
                    }
                }
                if ($found_index === false && $found_index !== 0) {
                    $permissions = $admin_permissions[$request['controller']]['permissions'];
                    //debug($permissions);

                    View::share('admin_permissions', $permissions);
                    $this->admin_permissions = $permissions;
                }

                View::composer('admin.layout', function($view){
                    $view->with('menu', json_decode(Session::get('admin_menu'), true));
                });
            }
        }, array('except' => array('login', 'post_login', 'update_locations_ajax', 'get_warehouses_ajax')));
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('some.view');
    }
}
