<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 13/09/2018
 * Time: 11:50
 */

class Volumetry
{
    private $url;

    private $dry_box = [];
    private $cold_big_box = [];
    private $cold_small_box = [];
    private $batteries = [];
    private $adv = 0;

    /**
     * Volumetry constructor.
     */
    public function __construct(){
        //$this->url = Config::get('app.services_transport.v1.url').'';
        $this->_setConfig();
    }

    /**
     * Funcion publica para calcular y actualizar la cantidad de contenedores de un pedido
     * @param $route_id
     */
    public function getContainers($route_id){
        $orders = User::select('id')->where('route_id',$route_id)->get();
        if(count($orders)){
            foreach($orders as $order){
                $products = $this->getProducts($order->id);
                if(count($products)){
                    $containers = $this->_getContainers($products);
                    $this->updateOrder($order->id, $containers);
                }
            }
        }
    }

    /**
     * Actualiza las ordenes con el listado de contenedores
     * @param $order_id
     * @param $containers
     */
    private function updateOrder($order_id, $containers){
        try{
            \DB::beginTransaction();
            $order = User::find($order_id);
            $order->expected_containers = json_encode($containers);
            $order->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
        }
    }
}