<?php

use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;
use App\Models\UserCreditCard;

/**
 * Class User
 * @property UserAddress[] $userAddresses
 * @property string $first_name
 * @property string $email
 * @property string $last_name
 * @property string $phone
 * @property string $created_at
 * @property UserCreditCard[] $creditCards
 * @property int $id
 * @property bool $phone_validated_date
 */
class User extends \Illuminate\Database\Eloquent\Model implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait, \users\UserAttributes;
    /**
     * @var string
     */
    protected $table = 'users';
    /**
     * @var array
     */
    protected $hidden = ['password'];
    /**
     * @var mixed
     */
    protected $ordersTotal = null;
    /**
     * @var mixed
     */
    protected $discountTotal = null;

    protected static function boot()
    {
        parent::boot();
        self::creating(function (User $model) {

            try {
                $foo = 'BAR';
                // some logic here!!
            } catch (Exception $e) {
                ErrorLog::add($e);
            } catch (Exception $e) {
                ErrorLog::add($e);
            }
        });
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('orders\Order');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relatedUser()
    {
        return $this->hasMany('User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referredBy()
    {
        return $this->belongsTo('User', 'referred_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return mixed
     */
    public function lastCall()
    {
        return $this->hasOne(TicketCall::class);
    }

    /**
     * @return mixed
     */
    public function firstCall()
    {
        return $this->hasOne('Ticket');
    }

    public function fooMethod()
    {
        $order = new \orders\Order();
        $order->create(['some' => 'data']);
    }

    public function getCreditAvailable()
    {
        return UserCredit::getCreditAvailable($this->id);
    }

    public function getAddresses()
    {
        DB::select('users.name AS user');
    }

    public function someMethod()
    {
        UserCredit::select('a', 'b')->lists('warehouse','id');
    }

    public function newPivot(Model $parent, array $attributes, $table, $exists)
    {
        return parent::newPivot($parent, $attributes, $table, $exists);
    }
}
