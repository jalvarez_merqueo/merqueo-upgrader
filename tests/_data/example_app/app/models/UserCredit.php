<?php

/**
 * Class UserCredit.
 */
class UserCredit extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = 'user_credits';

    public function users()
    {
        return $this->hasMany(\User::class);
    }
}
