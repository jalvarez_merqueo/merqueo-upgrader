<?php
namespace App\Actions;

use App\Traits\Transformers;
use Illuminate\Filesystem\Filesystem;

/**
 * Class UpgradeAppAction.
 *
 * @author Johan Alvarez <llstarscreamll@hotmail.com>
 */
class UpgradeAppAction
{
    use Transformers;

    /**
     * @var string
     */
    private $inputModelsPath;

    /**
     * @var string
     */
    private $inputControllersPath;

    /**
     * @var Illuminate\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * @var array
     */
    private $classesNamespacesFound = [];

    /**
     * @var bool
     */
    private $forceUcfirst = true;

    /**
     * @var array
     */
    private $appNamespaces = [
        'Models' => "App\Models\\",
        'Controllers' => "App\Http\Controllers\\",
        'Exceptions' => "App\Exceptions\\",
        'Classes' => "App\Classes\\",
        'Commands' => "App\Console\Commands\\",
        'Events' => "App\Events\\",
        'Observers' => "App\Observers\\",
    ];

    /**
     * Modelos que son "Authenticatables" en la app.
     */
    private $authenticatableModels = [
        'User.php',
        'Admin.php',
    ];

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->inputModelsPath=base_path(                   $this->     inputRootPath().       'app/models');
        $this->inputControllersPath=base_path(                  $this->     inputRootPath().       'app/controllers');
        $this->inputExceptionsPath=base_path(                   $this->     inputRootPath().       'app/exceptions');
        $this->inputClassesPath=base_path(                  $this->     inputRootPath().       'app/classes');
        $this->inputCommandsPath=base_path(                 $this->     inputRootPath().       'app/commands');
        $this->inputEventsPath=base_path(                   $this->     inputRootPath().       'app/events');
        $this->inputObserversPath=base_path(                    $this->     inputRootPath().       'app/observers');
        $this->inputViewsPath=base_path(                    $this->     inputRootPath().       'app/views');

        foreach ($this->appNamespaces as $classType => $namespacePrefix) {
            $this->classesNamespacesFound[$classType] = $this->collectFilesNamespaces($this->{"input{$classType}Path"}, $namespacePrefix);
        }
    }

    /**
     * Actualiza los modelos.
     */
    public function upgradeModels()
    {
        $this->upgradeDir($this->inputModelsPath, $this->outputModelsPath(), 'App\Models');
    }

    /**
     * Actualiza los controladores.
     */
    public function upgradeControllers()
    {
        $this->upgradeDir($this->inputControllersPath, $this->outputControllersPath(), 'App\Http\Controllers');
    }

    /**
     * Actualiza las excepciones.
     */
    public function upgradeExceptions()
    {
        $this->upgradeDir($this->inputExceptionsPath, $this->outputExceptionsPath(), 'App');
    }

    /**
     * Actualiza ficheros dentro de carpeta 'classes'.
     */
    public function upgradeClasses()
    {
        $this->upgradeDir($this->inputClassesPath, $this->outputClassesPath(), 'App\Classes');
    }

    /**
     * Actualiza los comandos.
     */
    public function upgradeCommands()
    {
        $this->upgradeDir($this->inputCommandsPath, $this->outputCommandsPath(), 'App\Console\Commands');
    }

    /**
     * Actualiza los eventos.
     */
    public function upgradeEvents()
    {
        $this->upgradeDir($this->inputEventsPath, $this->outputEventsPath(), 'App\Events');
    }

    /**
     * Actualiza los eventos.
     */
    public function upgradeObservers()
    {
        $this->upgradeDir($this->inputObserversPath, $this->outputObserversPath(), 'App\Observers');
    }

    /**
     * Actualiza las vistas.
     */
    public function upgradeViews()
    {
        $this->upgradeDir($this->inputViewsPath, $this->outputViewsPath());
    }

    /**
     * @param string $inputDir
     * @param string $outputDir
     */
    private function upgradeDir(string $inputDir,
        string $outputDir, string $namespacePrefix = '') {

        $files = $this->filesystem->allFiles($inputDir);

        collect($files)
            ->each(function ($file) use ($inputDir, $outputDir, $namespacePrefix) {
                $fileContents = $this->filesystem->get($file->getPathname());

                $fileContents = empty($namespacePrefix)
                    ? $fileContents
                    :
                $this->setNamespaceToFile($fileContents, $namespacePrefix);

                $fileContents = $this->fixNamespacesOnActionCalls(
                    $fileContents);
                $fileContents=$this->fixExceptionNamespace($fileContents);
                $fileContents=$this->fixEloquentRelationsNamespaces($fileContents);
                $fileContents=$this->fixRequestFacadeNamespace($fileContents);
                $fileContents=$this->fixEloquentMethods($fileContents);
                $fileContents=$this->fixNamespacesOnEventsListenFunctions($fileContents);

                // corregimos modelos "authenticatables", si aplica
                if (in_array($file->getFilename(), $this->authenticatableModels)) {
                    $fileContents = $this->fixAuthenticatableModel($fileContents);
                }

                foreach ($this->appNamespaces as $prefix) {
                    $fileContents = $this->fixNamespacesOnTopUseStatements($fileContents, $prefix);
                    $fileContents = $this->fixNamespacesOnUseDeclarations($fileContents, $prefix);
                    $fileContents = $this->fixNamespacesOnNewClassInstantiations($fileContents, $prefix);
                    $fileContents = $this->fixNamespacesOnClassesStaticCalls($fileContents, $prefix);
                    $fileContents = $this->fixNamespacesOnClassesTypeHints($fileContents, $prefix);
                    $fileContents = $this->fixNamespacesOnExtendClassDefinitions($fileContents, $prefix);
                }

                $fileContents = $this->changeFilterToMiddlewareOnControllers($fileContents);
                $fileContents = $this->fixNamespaceOnDbStaticCalls($fileContents);
                $fileContents = $this->changeFireMethodToHandle($fileContents);
                $fileContents = $this->addCsrfTokenToPostForms($fileContents);
                $fileContents = $this->changeGetEnvToEnv($fileContents);
                $fileContents = $this->fixInputFacadeCalls($fileContents);

                $fileContents = $this->fixUrlFacadeNamespace($fileContents);
                $fileContents = $this->fixBladeTemplateStuff($fileContents);
                $fileContents = $this->fixCountOnNoCountable($fileContents);
                $fileContents = $this->fixPaginatorRenderCalls($fileContents);

                // casos específicos para Merqueo
                $fileContents = $this->changeAuthBackendFacadeToAuth($fileContents);
                $fileContents = $this->changeRegexOnSomeControllersMiddleware($fileContents);
                $fileContents = str_replace('use JsonSchema\Constraints\Object;', '', $fileContents);
                $fileContents = str_replace('{{ $links_html }}', '{!! $links_html !!}', $fileContents);

                $fileContents = $this->fixAnotherFacadeCalls($fileContents);

                $this->writeFile($file, $outputDir, $fileContents);
            });
    }

    /**
     * @param $file
     */
    private function writeFile($file, string $outputPath, string $fileContents)
    {
        // En algunos casos no es necesario usar cammel
        // case en el nombre de los archivos.
        $relativePath = $file->getRelativePathname();

        if ($this->forceUcfirst) {
            // relative path from app/Models
            $relativePath = $this->normalizePathNames($file->getRelativePathname());
        }

        $segments = explode("/", $relativePath);

        array_pop($segments);
        $fileDir = $outputPath.implode("/", $segments);
        $outputPath = "{$outputPath}$relativePath";

        $this->createDirectory($fileDir);
        $this->filesystem->put($outputPath, $fileContents);
    }

    /**
     * Recolecta los namespaces finales de todas las clases que van a ser
     * actualizadas para tenerlas disponibles en los posteriores pasos y así
     * actualizar las referencias a estas clases en los ficheros.
     *
     * @param  string  $dir
     * @param  string  $namespacePrefix
     * @return array
     */
    private function collectFilesNamespaces(string $dir, string $namespacePrefix) : array
    {
        $files = $this->filesystem->allFiles($dir);

        return collect($files)
            ->map(function ($file) use ($namespacePrefix) {
                // recolecta namespaces de modelos encontrados
                $relativePath = $this->normalizePathNames($file->getRelativePathname());
                $namespace = str_replace(['/', '.php'], ['\\', ''], $relativePath);
                $namespace = $namespacePrefix.$namespace;

                return $namespace;
            })->all();
    }

    /**
     * @param  string  $contents
     * @return mixed
     */
    private function fixExceptionNamespace(string $contents): string
    {
        $pattern = ['/catch.?\(Exception \$/', '/extends Exception/'];
        $replacement = ["catch (\Exception $", 'extends \Exception'];
        $contents = preg_replace($pattern, $replacement, $contents);

        return $contents;
    }

    /**
     * Corrige los namespaces de los modelos dentro de los ficheros, como en las
     * relaciones, llamadas estáticas, nuevas instancias, declaraciones de
     * namespaces al que pertenecen en los ficheros, etc.
     *
     * @param  string  $contents
     * @return mixed
     */
    public function fixModelsNamespaces(string $contents): string
    {
        $regexForModelsToTransform = [
            '/\\\App\\\Models\\\[\w|\\\]+/',
            '/App\\\Models\\\[\w|\\\]+/',
        ];

        foreach ($regexForModelsToTransform as $regex) {
            preg_match($regex, $contents, $founds);

            foreach ($founds as $found) {
                $fixedModel = $this->normalizePathNames($found, "\\");
                $contents = str_replace($found, $fixedModel, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixEloquentRelationsNamespaces(string $contents): string
    {
        $relationsTypes = ['hasMany', 'belongsTo', 'hasOne'];
        $modelsNamespace = '\\App\Models\\';

        foreach ($relationsTypes as $relationType) {
            $pattern = [
                "/{$relationType}.?\((?!$modelsNamespace\)[\\\{1}]?([\w|\\\]+)::class/",
                "/{$relationType}.?\('([\w|\\\]+)'/",
            ];

            $replacement = "$relationType($1::class";
            $contents = preg_replace($pattern, $replacement, $contents);
        }

        $contents = $this->fixModelsNamespaces($contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixAuthenticatableModel(string $contents): string
    {
        // remuevo contratos antiguos
        $oldContracts = ['UserInterface', 'RemindableInterface'];
        foreach ($oldContracts as $oldContract) {
            $patterns = ["/implements\ {$oldContract},/", "/implements([\w\ ,\\\]+){$oldContract},/", "/implements([\w\ ,\\\]+){$oldContract}\n/"];
            $replacements = ["implements ", "implements \${1}", "implements \${1}\n"];
            $contents = preg_replace($patterns, $replacements, $contents);
        }

        // añado nuevos contratos
        $newContracts = implode(', ', ['AuthenticatableContract', 'AuthorizableContract', 'CanResetPasswordContract']);
        $contents = preg_replace("/implements.+\n/", "implements $newContracts\n", $contents);

        // añado sentencia use de contratos antiguos
        $authContractsUseStatements = [
            'use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;',
            'use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;',
            'use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;',
        ];

        foreach ($authContractsUseStatements as $useStatement) {
            $pattern = "/<\?php\n(namespace [\w|\\\]+;)?/";
            $replacement = "<?php\n\${1}\n$useStatement";
            $contents = preg_replace($pattern, $replacement, $contents);
        }

        // remuevo traits antiguos
        $oldTraits = [
            'RemindableTrait',
            'UserTrait',
        ];
        foreach ($oldTraits as $oldTrait) {
            $patterns = ["/(\{\n\s.+ )use([\w\ ,\\\]+){$oldTrait},/", "/(\{\n\s.+ )use([\w\ ,\\\]+) {$oldTrait};/"];
            $replacement = ["\${1}use\${2}", "\${1}use\${2};"];
            $contents = preg_replace($patterns, $replacement, $contents);
        }

        // añado nuevos traits
        $newTraits = implode(', ', [
            '\Illuminate\Auth\Authenticatable',
            '\Illuminate\Auth\Passwords\CanResetPassword',
            '\Illuminate\Foundation\Auth\Access\Authorizable',
            '\Illuminate\Notifications\Notifiable',
        ]);

        preg_match("/(\{\n\s.+ )use([\w\ \\\,]+);\n/", $contents, $useStatement);
        $patterns = ["/(\{\n\s.+ )use([ ,]+);\n/", "/(\{\n\s.+ )use (?!\\\Illuminate)([\w \\\,]+);\n/"];
        $replacements = ["\${1}use $newTraits;\n", "\${1}use \${2}, $newTraits;\n"];
        $contents = preg_replace($patterns, $replacements, $contents);
        // limpiamos excesos de espacios luego del use
        $contents = preg_replace("/(\{\n\s.+ )use([ ]+)(\w|\\\)/", '${1}use ${3}', $contents);

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnNewClassInstantiations(string $contents, string $namespacePrefix): string
    {
        if (preg_match('/class [\w]+ ?/', $contents) === 0) {
            // caso especial para vista donde se crea nueva instancia de modelo
            $contents = str_replace('=> new StoreProduct()', '=> new \App\Models\StoreProduct()', $contents);

            return $contents;
        }

        $regex = '/new\ [\w|\\\]+([\(|;|\)])/';
        preg_match_all($regex, $contents, $founds);

        foreach (array_unique(array_get($founds, '0')) as $key => $found) {
            $end = array_get($founds, "1.$key");
            $oldClass = preg_replace(['/new\ ?/', '/\(/', "/\\$end/"], '', $found);

            if ($this->existsClass($oldClass, $namespacePrefix)) {
                $class = trim($this->normalizePathNames($oldClass, '\\'), '\\');
                $foundFixed = "new \\$namespacePrefix$class".$end;
                $contents = str_replace($found, $foundFixed, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnExtendClassDefinitions(string $contents, string $namespacePrefix): string
    {
        $regex = '/class [\w]+ extends ([\w|\\\]+)[\n| |{]/';
        preg_match_all($regex, $contents, $founds);

        foreach (array_get($founds, '0') as $found) {
            $extendedClass = preg_replace(['/class [\w]+ extends /', "/\n| /"], '', $found);

            if ($this->existsClass($extendedClass, $namespacePrefix)) {
                $extendedClassFixed = '\\'.$namespacePrefix.trim($this->normalizePathNames($extendedClass, '\\'), '\\');
                $contents = str_replace($extendedClass, $extendedClassFixed, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnClassesStaticCalls(string $contents, string $namespacePrefix): string
    {
        $regex = "/[\w|\\\]+::(?!class)/";
        preg_match_all($regex, $contents, $founds);

        $foundClasses = array_unique(array_get($founds, '0'));
        $sortByLength = function ($a, $b) {
            return strlen($b) - strlen($a);
        };

        usort($foundClasses, $sortByLength);

        foreach ($foundClasses as $key => $found) {
            $oldClass = preg_replace(['/::/', '/ /'], '', $found);
            $oldClass = trim($oldClass, '\\');
            if ($this->existsClass($oldClass, $namespacePrefix)) {
                $class = trim($this->normalizePathNames($oldClass, '\\'), '\\');
                $oldClass = str_replace('\\', '\\\\', $oldClass);
                $pattern = "/( |\()\\\?$oldClass::(?!class)/";
                $replacement = "$1\\{$namespacePrefix}$class::";

                $contents = preg_replace($pattern, $replacement, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnClassesTypeHints(string $contents, string $namespacePrefix): string
    {
        $regex = '/[\ |\(](?!'.$namespacePrefix.'\)[\w|\\\]+\ \$/';
        preg_match_all($regex, $contents, $founds);

        foreach (array_collapse($founds) as $found) {
            $oldClass = preg_replace(['/\$/', '/\ /', '/\(/'], '', $found);

            if ($this->existsClass($oldClass, $namespacePrefix)) {
                $firstCharacter = substr($found, 0, 1);
                $class = trim($this->normalizePathNames($oldClass, '\\'), '\\');
                $foundFixed = "{$firstCharacter}\\{$namespacePrefix}$class $";

                $contents = str_replace($found, $foundFixed, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnUseDeclarations(string $contents, string $namespacePrefix): string
    {
        $regex = '/\{\n\s+ use[\w|\ |,|\\\]+([,|;])/';
        preg_match_all($regex, $contents, $useStatements);

        foreach (array_get($useStatements, '0') as $useStatement) {
            $oldClasses = preg_replace(['/use /', '/;/', '/\ /', '/{/', '/\n/'], '', $useStatement);
            $oldClasses = explode(',', $oldClasses);
            $useStatementFixed = $useStatement;

            foreach ($oldClasses as $oldClass) {
                $normalizedOldClass = $this->normalizePathNames($oldClass, '\\');
                if ($this->existsClass($normalizedOldClass, $namespacePrefix)) {
                    $fixedClass = trim($this->normalizePathNames($normalizedOldClass, '\\'), '\\');
                    $fixedClass = "\\$namespacePrefix$fixedClass";
                    $useStatementFixed = str_replace($oldClass, $fixedClass, $useStatement);
                }
            }

            $contents = str_replace($useStatement, $useStatementFixed, $contents);
        }

        return $contents;
    }

    /**
     * @param  string   $contents
     * @return string
     */
    private function fixNamespacesOnTopUseStatements(string $contents, string $namespacePrefix): string
    {
        $regex = '/^use [\w|\\\]+;/m';
        preg_match_all($regex, $contents, $topUseStatements);

        foreach (array_get($topUseStatements, '0') as $useStatement) {
            $oldClass = preg_replace(['/use/', '/;/', '/ /'], '', $useStatement);
            $useStatementFixed = $useStatement;
            $normalizedOldClass = $this->normalizePathNames($oldClass, '\\');

            if ($this->existsClass($normalizedOldClass, $namespacePrefix)) {
                $fixedClass = trim($this->normalizePathNames($normalizedOldClass, '\\'), '\\');
                $fixedClass = "$namespacePrefix$fixedClass";
                $useStatementFixed = str_replace($oldClass, $fixedClass, $useStatement);
            }

            $contents = str_replace($useStatement, $useStatementFixed, $contents);
        }

        return $contents;
    }

    /**
     * Fija el namespace a la clase de un archivo, para modelos.
     *
     * @param  string   $contents
     * @return string
     */
    private function setNamespaceToFile(string $contents, string $prefix): string
    {
        if (str_contains($contents, 'namespace ')) {
            $contents = preg_replace('/namespace ([\w|\\\]+);/', 'namespace '.$prefix.'\\\$1;', $contents);
            preg_match('/namespace ([\w|\\\]+);/', $contents, $founds);

            $namespace = str_replace(['namespace ', ';'], '', array_get($founds, '0'));
            $contents = str_replace($namespace, $this->normalizePathNames($namespace, '\\'), $contents);
        } else {
            $contents = preg_replace('/<\?php/', "<?php\nnamespace $prefix;", $contents);
        }

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixNamespaceOnDbStaticCalls(string $contents): string
    {
        $pattern = ["/(\\\?)DB::/"];
        $replacement = '\Illuminate\Support\Facades\DB::';
        $contents = preg_replace($pattern, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function changeFireMethodToHandle(string $contents): string
    {
        $pattern = ["/public function fire\(/"];
        $replacement = 'public function handle(';
        $contents = preg_replace($pattern, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function addCsrfTokenToPostForms(string $contents): string
    {
        $pattern = ["/<form([\s\S]*?)\">/"];
        $replacement = "<form\${1}\">\n@csrf";
        $contents = preg_replace($pattern, $replacement, $contents);

        return $contents;
    }

    /**
     * Actualiza el uso de beforeFilter y afterFilter de Laravel 4.2 a
     * middleware en Laravel 5.0, los middleware deben retornar siempre la
     * petición procesada. En los cambios aquí realizados se espera de forma
     * optimista que solo haya un middleware por clase.
     *
     * @param string $contents
     */
    private function changeFilterToMiddlewareOnControllers(string $contents): string
    {
        // los middleware en Laravel deben retornar la petición en curso siempre
        $startFunctionPatterns = [
            'this->beforeFilter ?\([\s|\n]?function ?\(\)[.|\s]+?{',
            'this->beforeFilter ?\([\s|\n]+?function ?\(\)[.|\s]+?{',
        ];

        $endFunctionPatterns = ['(},)', '(}\);)']; // fin de función anónima y primer argumento de middleware

        foreach ($startFunctionPatterns as $startFunctionPattern) {
            // expresión regular tomada de:
            // https://stackoverflow.com/questions/38110833/match-the-body-of-a-function-using-regex/38111480#38111480
            $functionBodyPattern = '((?>[^{}]++|(?R)|{|})*)'; // cuerpo de función anónima

            foreach ($endFunctionPatterns as $endFunctionPattern) {
                $pattern = '~'.$startFunctionPattern.$functionBodyPattern.$endFunctionPattern.'~';
                $replacement = "this->middleware(function (\$incomingRequest, \$next) {\${1}return \$next(\$incomingRequest);\n\${2}";
                $contents = preg_replace($pattern, $replacement, $contents);
            }
        }

        $handleRequest = "\$incomingRequestResponse = \$next(\$incomingRequest);";
        $returnHandledRequest = "return \$incomingRequestResponse;";

        foreach ($endFunctionPatterns as $endFunctionPattern) {
            $startFunctionPattern = 'this->afterFilter ?\(function ?\(\)[.|\s]+?{';
            $pattern = '~'.$startFunctionPattern.$functionBodyPattern.$endFunctionPattern.'~';
            $replacement = "this->middleware(function (\$incomingRequest, \$next) {\n$handleRequest\${1}$returnHandledRequest\n\${2}";
            $contents = preg_replace($pattern, $replacement, $contents);
        }

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function changeGetEnvToEnv(string $contents): string
    {
        $patterns = ['/getenv\(/'];
        $replacement = 'env(';
        $contents = preg_replace($patterns, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixInputFacadeCalls(string $contents): string
    {
        $requestFacadeNamespace = '\\\Illuminate\Support\Facades\Request';
        $patterns = ['/\\\?Input::get\(\)/', '/\\\?Input::has/', '/\\\?Input::/'];
        $replacement = ["$requestFacadeNamespace::all()", "$requestFacadeNamespace::filled", "$requestFacadeNamespace::"];
        $contents = preg_replace($patterns, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixUrlFacadeNamespace(string $contents): string
    {
        $urlFacadeNamespace = '\\\Illuminate\Support\Facades\URL';
        $patterns = ['/(?!\\\)URL::/'];
        $replacement = ["$urlFacadeNamespace::"];
        $contents = preg_replace($patterns, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixBladeTemplateStuff(string $contents): string
    {
        $patternsMap = [
            '/; }}/' => ' }}',
            '/;}}/' => '}}',
            '/<head>/' => "<head>\n<meta name=\"csrf-token\" content=\"{{ csrf_token() }}\">",
            '/{{ ?\$([\w|_]+)->toJson\(\) ?}}/' => '{!! $${1}->toJson() !!}',
            '/{{ ?Form::select\((.+), ?true\)/' => '{{ Form::select(${1})',
            '/action\(\$menu_item\[\'action\'\]\)/' => 'action(normalizeControllerAction($menu_item[\'action\']))',
            '/action\(\$submenu\[\'action\'\]\)/' => 'action(normalizeControllerAction($submenu[\'action\']))',
            '/\] \+ \$admins/' => '] + $admins->toArray()',
            '/{{ \$validation_reasons }}/' => '{!! $validation_reasons !!}',
            '/{{ \$delivery_times }}/' => '{!! $delivery_times !!}',
            '/{{ \$stores }}/' => '{!! $stores !!}',
        ];

        $contents = preg_replace(array_keys($patternsMap), $patternsMap, $contents);

        return $contents;
    }

    /**
     * @param  string  $contents
     * @return mixed
     */
    private function fixNamespacesOnEventsListenFunctions(string $contents): string
    {
        $patternsMap = [
            "/listen\((\s+? +?.+\s+? +')/" => 'listen($1App\Events\\',
            "/listen\(('\w+\.\w+', ?')/" => 'listen($1App\Events\\',
        ];

        $contents = preg_replace(array_keys($patternsMap), $patternsMap, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixCountOnNoCountable(string $contents): string
    {
        $patternsMap = [
            '/if ?\(count\((\$[\w]+)\)/' => 'if((is_array($1) || $1 instanceof Countable) && count($1)',
            '/{{ ?count\((\$[\w]+)\)/' => '{{ is_array($1) || $1 instanceof Countable ? count($1) : 0',
        ];

        $contents = preg_replace(array_keys($patternsMap), $patternsMap, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixAnotherFacadeCalls(string $contents): string
    {
        $patternsMap = [
            '/Session::set\(/' => 'Session::put(',
            '/new StdClass/' => 'new \StdClass',
            '/\\\?Session::/' => '\Illuminate\Support\Facades\Session::',
            '/\\\?Config::/' => '\Illuminate\Support\Facades\Config::',
            '/\\\?View::/' => '\Illuminate\Support\Facades\View::',
            '/\\\?Cookie::/' => '\Illuminate\Support\Facades\Cookie::',
            '/\\\?Redirect::/' => '\Illuminate\Support\Facades\Redirect::',
            '/\\\?Route::/' => '\Illuminate\Support\Facades\Route::',
            '/\\\?Response::/' => '\Illuminate\Support\Facades\Response::',
            '/->getParameter\(/' => '->parameter(',
            '/\\\?Carbon\\\Carbon/' => '\Carbon\Carbon',
            '/::remember\([\d]+\)->/' => '::',
            '/(?![Facades])\\\?Auth::/' => '\Illuminate\Support\Facades\Auth::',
            '/new DateTime/' => 'new \DateTime',
            '/new Pusher\\\Pusher/' => 'new \Pusher\Pusher',
            '/->getPath\(\)/' => '->uri()',
            '/{{ ?Breadcrumb::(\w+.+)}}/' => '{!! Breadcrumb::$1 !!}',
            '/{{ \\\Illuminate\\\Support\\\Facades\\\View::make\((.+) ?}}/' => '{!! \Illuminate\Support\Facades\View::make($1 !!}',
            // en caso de que si se ha llegado a este punto no se han logrado cubrir todos los casos de filters
            '/\$this->beforeFilter\(function \(\) {/' => '$this->middleware(function ($incomingRequest, $next) {',
            '/}, \$rules\);/' => "return \$next(\$incomingRequest);\n}, \$rules);",
        ];

        $patterns = array_keys($patternsMap);
        $contents = preg_replace($patterns, $patternsMap, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixPaginatorRenderCalls(string $contents): string
    {
        $patterns = ['/->links\(\)[ |;| ;]?}}/'];
        $replacement = '->render() }}';

        if (str_contains($contents, "\nclass ")) {
            $patterns = '/->links\(\)->render\(\)/';
            $replacement = '->render()->toHtml()';
        }

        $contents = preg_replace($patterns, $replacement, $contents);

        // otros métodos cambiados en paginador en Laravel 5.0
        $patternsMap = [
            '/->getFrom\(\)/' => '->firstItem()',
            '/->getTo\(\)/' => '->lastItem()',
            '/->getPerPage\(\)/' => '->perPage()',
            '/->getCurrentPage\(\)/' => '->currentPage()',
            '/->getLastPage\(\)/' => '->lastPage()',
            '/->getTotal\(\)/' => '->total()',
        ];
        $patterns = array_keys($patternsMap);
        $contents = preg_replace($patterns, $patternsMap, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixEloquentMethods(string $contents): string
    {
        $patternsMap = [
            '/->pluck\(/' => '->value(',
            '/->lists\(/' => '->pluck(',
            '/ newPivot\(([\$|\w|\s|,]+)\)/' => ' newPivot($1, $using = null)',
            '/extends Eloquent/' => 'extends \Illuminate\Database\Eloquent\Model',
        ];

        $patterns = array_keys($patternsMap);
        $replacements = array_values($patternsMap);
        $contents = preg_replace($patterns, $replacements, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixRequestFacadeNamespace(string $contents): string
    {
        $requestFacadeNamespace = '\\\Illuminate\Support\Facades\Request';
        $patterns = ['/(?!\\\)Request::/'];
        $replacement = ["$requestFacadeNamespace::"];
        $contents = preg_replace($patterns, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function fixNamespacesOnActionCalls(string $contents): string
    {
        $regex = "/action\(([\s]+)?('|\")([\w|\\\]+)/";
        preg_match_all($regex, $contents, $founds);

        foreach (array_get($founds, '0') as $key => $found) {
            $secondCapturingGroup = array_get($founds, "2.$key");
            $thirdCapturingGroup = array_get($founds, "3.$key");
            $oldClass = $thirdCapturingGroup;
            $namespacePrefix = 'App\Http\Controllers\\';

            if ($this->existsClass($oldClass, $namespacePrefix)) {
                $class = trim($this->normalizePathNames($oldClass, '\\'), '\\');
                $firstString = substr($found, 0, 1);
                $foundFixed = "action({$secondCapturingGroup}\\{$namespacePrefix}$class";
                $contents = str_replace($found, $foundFixed, $contents);
            }
        }

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function changeAuthBackendFacadeToAuth(string $contents): string
    {
        $authFacadeNamespace = '\\\Auth'; // el namespace completo es fijado en otro paso
        $patterns = ['/(?!\\\)AuthBackend::/', '/(?!\\\)BackendAuth::/'];
        $replacement = "$authFacadeNamespace::guard('admin')->";
        $contents = preg_replace($patterns, $replacement, $contents);

        return $contents;
    }

    /**
     * @param string $contents
     */
    private function changeRegexOnSomeControllersMiddleware(string $contents): string
    {
        $find = "str_replace('admin\\\', '', \$request['controller'])";
        $replace = 'preg_replace([\'/admin\\\\\\/\', \'/^\\\\\\/\'], \'\', $request[\'controller\'])';
        $contents = str_replace($find, $replace, $contents);

        return $contents;
    }

    /**
     * @param  string $oldClassStyle
     * @return bool
     */
    private function existsClass(string $oldClassStyle, string $namespacePrefix, $debug = false): bool
    {
        $separator = "\\";
        $class = $this->normalizePathNames($oldClassStyle, $separator);
        $class = $namespacePrefix.trim($class, '\\');

        $classes = array_collapse($this->classesNamespacesFound);

        return in_array($class, $classes) || in_array('\\'.$class, $classes);
    }

    /**
     * @param  bool               $forceUcfirst
     * @return UpgradeAppAction
     */
    public function setForceUcfirst(bool $forceUcfirst): UpgradeAppAction
    {
        $this->forceUcfirst = $forceUcfirst;

        return $this;
    }
}
