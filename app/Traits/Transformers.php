<?php
namespace App\Traits;

/**
 * Trait Transformers.
 *
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
trait Transformers
{
    private function normalizeAllPaths(string $fileContents)
    {
        collect($this->regexPathsToTransform)
            ->each(function () use (&$fileContents) {
                preg_match('/App\\\Models\\\[\w|\\\]+/', $contents, $founds);
            });
    }

    /**
     * @param string $dir
     */
    private function createDirectory(string $dir): void
    {
        $this->filesystem->makeDirectory($dir, $mode = 0755, $recursive = true, $force = true);
    }

    /**
     * @return string
     */
    protected function inputRootPath(): string
    {
        return config('upgrader.input_path');
    }

    /**
     * @return string
     */
    protected function outputRootPath(): string
    {
        return config('upgrader.output_path');
    }

    /**
     * @return string
     */
    protected function outputModelsPath(): string
    {
        return base_path($this->outputRootPath()."app/Models/");
    }

    /**
     * @return string
     */
    protected function outputControllersPath(): string
    {
        return base_path($this->outputRootPath()."app/Http/Controllers/");
    }

    /**
     * @return string
     */
    protected function outputExceptionsPath(): string
    {
        return base_path($this->outputRootPath()."app/Exceptions/");
    }

    /**
     * @return string
     */
    protected function outputClassesPath(): string
    {
        return base_path($this->outputRootPath()."app/Classes/");
    }

    /**
     * @return string
     */
    protected function outputCommandsPath(): string
    {
        return base_path($this->outputRootPath()."app/Console/Commands/");
    }

    /**
     * @return string
     */
    protected function outputEventsPath(): string
    {
        return base_path($this->outputRootPath()."app/Events/");
    }

    /**
     * @return string
     */
    protected function outputObserversPath(): string
    {
        return base_path($this->outputRootPath()."app/Observers/");
    }

    /**
     * @return string
     */
    protected function outputViewsPath(): string
    {
        return base_path($this->outputRootPath()."resources/views/");
    }

    /**
     * @param  string   $path
     * @return string
     */
    public function normalizePathNames(string $path, $delimiter = "/"): string
    {
        return collect(explode($delimiter, $path))
            ->map(function ($fragment) {
                return ucfirst($fragment);
            })->implode($delimiter);
    }
}
