<?php
namespace App\Commands;

use App\Actions\UpgradeAppAction;
use LaravelZero\Framework\Commands\Command;

/**
 * Class UpgradeCommand.
 *
 * @author Johan Alvarez <llstarscreamll@hotmail.com>
 */
class UpgradeCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'upgrade';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Upgrades Laravel app from 4.2 to 5.7';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = app(UpgradeAppAction::class);

        $action->setForceUcfirst(false);

        $this->task("Upgrading views", function () use ($action) {
            return $action->upgradeViews();
        });

        $action->setForceUcfirst(true);

        $this->task("Upgrading controllers", function () use ($action) {
            return $action->upgradeControllers();
        });

        $this->task("Upgrading models", function () use ($action) {
            return $action->upgradeModels();
        });

        $this->task("Upgrading exceptions", function () use ($action) {
            return $action->upgradeExceptions();
        });

        $this->task("Upgrading classes", function () use ($action) {
            return $action->upgradeClasses();
        });

        $this->task("Upgrading commands", function () use ($action) {
            return $action->upgradeCommands();
        });

        $this->task("Upgrading events", function () use ($action) {
            return $action->upgradeEvents();
        });

        $this->task("Upgrading observers", function () use ($action) {
            return $action->upgradeObservers();
        });
    }
}
