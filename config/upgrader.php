<?php

use App\Actions\FixNamespacesOnHasManyCallsAction;

return [
    /**
     * App to be upgraded paths.
     *
     * Paths must be relative to this app base path.
     */
    'input_path'  => env('INPUT_PATH', '../../laravel4.2/'),

    /**
     * Where the upgraded app should be located.
     */
    'output_path' => env('OUTPUT_PATH', 'storage/app/upgrades/'),
];
