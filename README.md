# Merqueo Upgrader

Aplicación escrita con Laravel Zero para realizar tareas rutinarias del upgrade de Laravel 4.2 a Laravel 5.7 del proyecto de Merqueo. Básicamente corrige todo el asunto de los namespaces de las clases, estructura de carpetas, funciones obsoletas y cambiadas y otras cosas específicas a Merqueo.

Cosas de las que hay que cuidar después del upgrade:

- Las llamadas al método `get()` en el query builder ahora retornan una instancia de `Illuminate\Support\Collection`, antes devolvía un array, así que se debe cuidar donde es usado dicho método.